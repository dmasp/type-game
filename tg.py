#!/usr/bin/python3

import pyttsx3
import random
import pygame

class Words():
    def __init__(self):
        # Read the words from a text file
        self.word_list = []
        self.typed_pos = 0
        self.success = False
        text_file = open("words.txt", "r")
        text = text_file.read()
        self.tts_engine = pyttsx3.init()
        voices = self.tts_engine.getProperty('voices')
        print()
        print()
        print("Available Text-to-Speech voices/languages.")
        print()
        for i, v in enumerate(voices):
            print(f"{i} - {v.name}")
        try:
            print()
            print("Choose a voice and language and type its number and press enter to use for Text-to-speech.")
            self.chosen_voice = int(input("Or type anything else to disable Text-to-speech: "))
            self.tts_engine.setProperty('voice', voices[self.chosen_voice].id)
            self.tts_engine.setProperty('rate', 125)
            self.say = True
        except:
            print("Proceeding without Text-to-Speech")
            self.say = False

        for word in text.split():
            self.word_list.append(word)
    
    def new_game(self):
        self.typed_pos = 0
        self.success = False

    def pick_word(self):
        # Return word from list
        self.current_word = random.choice(self.word_list)
        return self.current_word
    
    def get_typed_word(self):
        return self.current_word[:self.typed_pos]

    def get_success(self):
        return self.success
    
    def check_key(self, k):
        key = chr(k).upper()
        if (self.typed_pos >= len(self.current_word)):
            return

        if (key == self.current_word[self.typed_pos]):
            self.typed_pos += 1
            if (self.typed_pos == len(self.current_word)):
                self.success = True
    
    def say_word(self):
        if (self.say == True):
            self.tts_engine.say(self.current_word)
            self.tts_engine.runAndWait()

def word_complete():
    print("Complete!")

def main():
    words = Words()
    word = words.pick_word()
    pygame.init()
    screen_x = 1024
    screen_y = 768
    screen = pygame.display.set_mode((screen_x, screen_y))
    clock = pygame.time.Clock()
    done = False
    font = pygame.font.SysFont(None, 144)
    font_complete = pygame.font.SysFont(None, 72)
    game_complete = False
    words.say_word()

    while not done:
        screen.fill((255, 255, 255))
        typed_text = font.render(words.get_typed_word(), True, (255, 75, 75))
        text = font.render(word, True, (0, 128, 255))
        if (words.get_success() == True):
            game_complete = True
        if game_complete == True:
            text_complete = font_complete.render("N - New Word", True, (75, 125, 75))
            screen.blit(text_complete, ((screen_x // 2) - (text_complete.get_width() // 2), (screen_y // 2) - (text_complete.get_height() // 2)))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                done = True
            if event.type == pygame.KEYDOWN and event.key == pygame.K_8:
                words.say_word()
            if game_complete == False and event.type == pygame.KEYDOWN:
                words.check_key(event.key)
            if game_complete == True and event.type == pygame.KEYDOWN and event.key == pygame.K_n:
                word = words.pick_word()
                words.new_game()
                game_complete = False
                words.say_word()

        screen.blit(text, ((screen_x // 2) - (text.get_width() // 2), (screen_y // 3) - (text.get_height() // 2)))
        screen.blit(typed_text, ((screen_x // 2) - (text.get_width() // 2), ((screen_y // 3) * 2) - (text.get_height() // 2)))
        pygame.display.flip()
        clock.tick(60)

if __name__ == "__main__":
    main()